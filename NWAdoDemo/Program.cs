﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWAdoDemo
{
    partial class Category
    {
        public override string ToString()
        {
            return $"{this.CategoryID}: {this.CategoryName} ({this.Description})";
        }
    }

    class Program
    {
        static void Main(string[] args)
        {

            NWDataContext data = new NWDataContext();
            data.Log = Console.Out;  // see rida võimaldab näha tema päringuid

            var products = data.Products.ToList();
            foreach (var c in data.Categories.ToList()) // ilma selle tolistita teeb ta 8 eraldi päringut
            {
                Console.WriteLine(c);
                foreach (var p in products.Where(x => x.CategoryID == c.CategoryID))
                {

                    Console.WriteLine("\t {0}", p.ProductName);
                }

            }

            foreach (var p in data.Products)
            {
                // Console.WriteLine($"toode {p.ProductName} kuulub kategooriasse {p.Category.CategoryName}");
            }

            // see on näide, kuidas kasutada Stored Protseduure
            foreach (var x in data.Sales_by_Year(DateTime.Parse("1.1.1997"), DateTime.Parse("1.1.1998")))
            {
                Console.WriteLine("{1}: {2}: {0:F2}", x.Subtotal, ((DateTime)x.ShippedDate).ToShortDateString(), x.OrderID);
            }

            // päring, mis paneb kokku (join) kaks tabelit
            var q = from p in data.Products
                    join c in data.Categories
                    on p.CategoryID equals c.CategoryID
                    select new { c.CategoryName, p.ProductName, p.UnitPrice }
                    ;

            var qa = data.Products
                .Join(
                        data.Categories,
                        c => c.CategoryID,
                        p => p.CategoryID,
                        (p, c) => new { Cat = c, Prod = p })
                .Select(x => new { CategoryName = x.Cat.CategoryName, x.Prod.ProductName, x.Prod.UnitPrice })
                        ;

            var qb = data.Products
                .Join(
                        data.Categories,
                        c => c.CategoryID,
                        p => p.CategoryID,
                        (p, c) => new { c.CategoryName, p.ProductName, p.UnitPrice }
                        )
                        ;
            foreach (var x in q) Console.WriteLine(x);
            foreach (var x in qa) Console.WriteLine(x);
            foreach (var x in qb) Console.WriteLine(x);

            var qs = from p in data.Products
                     group p by p.CategoryID into g
                     select new
                     {
                         CategoryID = g.Key,
                         Laoseis = g.Sum(x => x.UnitPrice * x.UnitsInStock)
                     };

            var qu = data.Products
                    .GroupBy(p => p.CategoryID)
                    .Select(g => new
                    {
                        CategoryID = g.Key,
                        Laoseis = g.Sum(x => x.UnitPrice * x.UnitsInStock)
                    });

            foreach (var x in qs)
            {
                Console.WriteLine(x);
            }

        }
    }
}
